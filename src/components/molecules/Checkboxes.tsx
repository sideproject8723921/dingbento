import clsx from 'clsx'
import type { FunctionComponent } from 'react'
import type { UseControllerProps } from 'react-hook-form'
import { useController, useFormContext } from 'react-hook-form'

import { InputError } from '@/components/atoms'

interface OptionType {
  value: string
  label: string
  readOnly?: boolean
}

interface CheckboxesProps extends UseControllerProps {
  options: OptionType[]
  inputClass?: string
  className?: string
}

/**
 * 自定義 react-hook-form 勾選框 (必須寫在 Form 裡面)
 *
 * @param name html name 屬性
 * @param readOnly 是否唯獨
 * @param inputClass checkbox input 的額外 class
 * @param className 額外 class
 * @param options 勾選狂選項物件陣列
 * - value 勾選框的值
 * - label 勾選框顯示文字
 * - readOnly 勾選框是否唯獨
 */
const Checkboxes: FunctionComponent<CheckboxesProps> = ({
  name,
  options,
  inputClass,
  className,
  defaultValue = [],
}) => {
  const { control } = useFormContext()
  const { field, fieldState } = useController({
    name,
    control,
    defaultValue,
  })

  return (
    <div className={clsx(['relative w-max', className])}>
      {options.map(({ value, label, readOnly }) => (
        <label key={value} className={clsx({
          'cursor-pointer space-x-1': true,
          'text-red-500': fieldState.invalid,
          '!text-gray-400 !cursor-not-allowed': readOnly,
        })}>
          <input
            {...field}
            aria-label={`Checkbox for ${name}`}
            type="checkbox"
            className={clsx({
              'peer h-5 w-5 cursor-pointer border rounded-md bg-container-normal/50 text-primary transition-all !ring-transparent': true,
              'border-red-400 focus:border-red-400 focus:ring-offset-red-400/30': fieldState.invalid,
              'border-gray-300 focus:border-primary focus:ring-offset-primary/25': !fieldState.invalid,
              'bg-gray-200 border-gray-300 !cursor-not-allowed': readOnly,
              inputClass,
            })}
            checked={field.value.includes(value)}
            onChange={(e) => {
              if (readOnly)
                return
              let valueCopy = [...field.value]
              const checked = e.target.checked
              // 整理 value 陣列
              if (checked)
                valueCopy.push(value)
              else valueCopy = valueCopy.filter(v => v !== value)
              // 排序後更新到 react-hook-form
              field.onChange(valueCopy.sort((a, b) => a > b ? 1 : -1))
            }}
          />
          <span> {label} </span>
          {
            fieldState.error?.message
            && <InputError> { fieldState.error?.message } </InputError>
          }
        </label>
      ))}
    </div>
  )
}
Checkboxes.displayName = 'Checkboxes'

export default Checkboxes
