import Image from 'next/image'
import type { FunctionComponent, ReactNode } from 'react'

interface CardProps {
  image: string
  children: ReactNode
}

/**
 * 自定義卡片
 *
 * */
const Card: FunctionComponent<CardProps> = ({ image, children }) => {
  return (
    <div className='cursor-pointer rounded-md p-2 transition hover:bg-container-normal hover:shadow-lg'>
      <div className='relative h-32 w-full overflow-hidden rounded-md md:h-48'>
        <Image alt='Card image' src={image} fill={true} />
      </div>
      <div className='p-2 pb-0'>
        {children}
      </div>
    </div>
  )
}
Card.displayName = 'Card'

export default Card
