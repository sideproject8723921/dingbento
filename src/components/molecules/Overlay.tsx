import clsx from 'clsx'
import { AnimatePresence, m } from 'framer-motion'
import type { FunctionComponent, ReactNode } from 'react'
import { useEffect, useMemo } from 'react'

import { Portal } from '@/components/atoms'

interface OverlayProps {
  show: boolean
  keepFocusState?: boolean
  className?: string
  children?: ReactNode
}

/**
 * 半透明背景遮罩
 *
 * @param show 是否顯示
 * @param keepFocusState 遮罩顯示時，是否要保留原本 focus 的狀態
 * @param className 額外 class
 * @param children ReactNode
 */
const Overlay: FunctionComponent<OverlayProps> = ({
  show,
  keepFocusState,
  className,
  children,
}) => {
  const uniqueId = useMemo(() => String(Math.random() * 100000), [])

  useEffect(() => {
    if (show && !keepFocusState)
      (document.activeElement as HTMLElement).blur()
  }, [show, keepFocusState])

  return (
    <Portal>
      <AnimatePresence>
        {show
          && (
          <m.div
            key={uniqueId}
            initial={{ opacity: 0 }}
            animate={{ opacity: 1 }}
            exit={{ opacity: 0 }}
            transition={{ duration: 0.15 }}
            className={clsx({
              'fixed left-0 top-0 h-full w-full flex flex-col items-center justify-center bg-container-inverse/50 backdrop-blur-sm z-10': true,
              className,
            })}
          >
            {children}
          </m.div>
          )
        }
      </AnimatePresence>
    </Portal>
  )
}
Overlay.displayName = 'Overlay'

export default Overlay
