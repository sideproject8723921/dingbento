import clsx from 'clsx'
import type { FunctionComponent, ReactNode } from 'react'
import type { UseControllerProps } from 'react-hook-form'
import { useController, useFormContext } from 'react-hook-form'

import { InputError } from '@/components/atoms'

interface SelectProps extends UseControllerProps {
  placeholder?: string
  readOnly?: boolean
  selectClass?: string
  className?: string
  children: ReactNode[]
}

/**
 * 自定義 react-hook-form 下拉式欄位 (必須寫在 Form 裡面)
 *
 * @param name html name 屬性
 * @param placeholder 預留提示文字
 * @param readOnly 是否唯獨
 * @param selectClass Select 的額外 class
 * @param className 額外 class
 * @param children options
 */
const Select: FunctionComponent<SelectProps> = ({
  name,
  placeholder,
  readOnly,
  selectClass,
  className,
  children,
  defaultValue = '',
}) => {
  const { control } = useFormContext()
  const { field, fieldState } = useController({
    name,
    control,
    defaultValue,
  })

  return (
    <div className={clsx(['relative w-full', className])}>
      <select
        {...field}
        className={clsx({
          'peer w-full rounded-md border bg-container-normal/50 pl-3 pr-9 py-2 shadow-sm transition-all focus:ring-2 outline-none cursor-pointer bg-[right_0.25rem_center]': true,
          'border-red-400 focus:border-red-400 focus:ring-red-400/30': fieldState.invalid,
          'border-gray-300 focus:border-primary focus:ring-primary/25': !fieldState.invalid,
          'bg-gray-200 text-text-normal/50 !cursor-not-allowed': readOnly,
          [selectClass as string]: selectClass,
        })}
        onChange={(e) => {
          if (readOnly)
            return
          field.onChange(e)
        }}
        aria-readonly={readOnly}
      >
        <option disabled value="">
          {placeholder || '請選擇'}
        </option>
        {children}
      </select>
      {
        fieldState.error?.message
        && <InputError> { fieldState.error?.message } </InputError>
      }
    </div>
  )
}
Select.displayName = 'Select'

export default Select
