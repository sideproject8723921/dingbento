import type { ChangeEvent, FunctionComponent } from 'react'
import { useRef } from 'react'
import type { FieldValues, UseFormSetValue } from 'react-hook-form'
import * as xlsx from 'xlsx'

import { Button } from '@/components/atoms'
import { Input, Modal } from '@/components/molecules'
import type { ModalProps } from '@/components/molecules/Modal'
import { Form } from '@/components/organisms'
import z from '@/lib/zod'

const schema = z.object({
  name: z.string().min(1),
  telno: z.string().regex(/^\d+$/).min(8).max(10),
  address: z.string().min(1),
  fileName: z.string().min(1),
  fileData: z.any(),
})

type SchemaType = z.infer<typeof schema>

export interface UploadModalProps extends ModalProps {
  onSubmit: (formData: SchemaType) => void
}

const UploadModal: FunctionComponent<UploadModalProps> = ({
  onSubmit,
  ...props
}) => {
  const uploadInput = useRef<HTMLInputElement>(null)

  const triggerUpload = () => {
    uploadInput.current?.click()
  }

  const readExcel = (e: ChangeEvent<HTMLInputElement>, setValue: UseFormSetValue<FieldValues>) => {
    const file = e.target.files && e.target.files[0]
    if (!file)
      return console.error('File not exist!')

    const promise = new Promise((resolve, reject) => {
      const fileReader = new FileReader()
      fileReader.readAsArrayBuffer(file)
      fileReader.onload = (e) => {
        const bufferArray = e.target?.result
        const wb = xlsx.read(bufferArray, {
          type: 'buffer',
        })
        const wsname = wb.SheetNames[0]
        const ws = wb.Sheets[wsname]
        const data = xlsx.utils.sheet_to_json(ws)
        resolve(data)
      }
      fileReader.onerror = (error) => {
        reject(error)
      }
    })
    promise.then((data) => {
      setValue('fileName', file.name)
      setValue('fileData', data)
    })
  }

  return (
    <Modal {...props} className='max-w-md'>
      <Form
        className='space-y-3'
        schema={schema}
        onSubmit={onSubmit}
      >
        {
          ({ setValue }) => (
            <>
              <Input name='name' placeholder='名稱' />
              <Input name='telno' placeholder='電話' />
              <Input name='address' placeholder='地址' />
              <input
                type='file'
                accept=".xlsx"
                className='hidden'
                ref={uploadInput}
                onChange={e => readExcel(e, setValue)}
              />
              <div className='flex items-center justify-start space-x-1'>
                <Button ariaLabel='Upload' className='flex-none' onClick={triggerUpload}>上傳</Button>
                <Input name='fileName' placeholder='菜單 Excel 檔' readOnly />
              </div>
              <Button ariaLabel='Submit' className='ml-auto flex' type="submit">送出</Button>
            </>
          )
        }
      </Form>
    </Modal>
  )
}
UploadModal.displayName = 'UploadModal'

export default UploadModal
