import type { FunctionComponent, ReactNode } from 'react'
import { createPortal } from 'react-dom'
import { useIsClient } from 'usehooks-ts'

interface PortalProps {
  to?: HTMLElement
  children: ReactNode
}

/**
 * Nextjs 直接用 react-dom portal 會有 SSR 的問題，要補一層 wrapper 確保在 client 端才渲染
 *
 * @param to 要 portal 到哪個 dom element，預設是 body
 * @param children ReactNode
 */
const Portal: FunctionComponent<PortalProps> = ({ to, children }) => {
  const isClient = useIsClient()
  return isClient ? createPortal(children, to || document.body) : null
}
Portal.displayName = 'Portal'

export default Portal
