import { faMoon, faSun } from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import clsx from 'clsx'
import type { FunctionComponent } from 'react'

interface DarkmodeSwitchProps {
  isDarkMode: boolean
  toggle: () => void
}

/**
 * 亮、暗色系切換 switch
 *
 * @param isDarkMode from useDarkMode
 * @param toggle from useDarkMode
 */
const DarkmodeSwitch: FunctionComponent<DarkmodeSwitchProps> = ({
  isDarkMode,
  toggle,
}) => {
  return (
    <div
      className='relative flex w-16 cursor-pointer items-center justify-between rounded-full border bg-container-inverse/10 p-2'
      onClick={toggle}
    >
      <FontAwesomeIcon icon={faSun}/>
      <FontAwesomeIcon icon={faMoon}/>
      <div className={clsx({
        'absolute top-1/2 -translate-y-1/2 left-0 h-[calc(100%-2px)] aspect-square rounded-full bg-primary transition-all': true,
        'translate-x-[1px]': isDarkMode,
        'translate-x-[31px]': !isDarkMode,
      })}></div>
    </div>
  )
}
DarkmodeSwitch.displayName = 'DarkmodeSwitch'

export default DarkmodeSwitch
