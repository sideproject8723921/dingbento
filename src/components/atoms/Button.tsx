import clsx from 'clsx'
import type { AriaAttributes, MouseEventHandler, ReactNode } from 'react'
import { forwardRef } from 'react'

interface ButtonProps {
  type?: 'button' | 'submit'
  variant?: 'default' | 'link' | 'header'
  color?: 'primary' | 'secondary' | 'gradient' | 'red' | 'green' | 'transparent'
  size?: 'sm' | 'md' | 'lg'
  className?: string
  onClick?: MouseEventHandler
  ariaLabel: AriaAttributes['aria-label']
  children: ReactNode
}

/**
 * 自定義按鈕
 *
 * @param type html 按鈕類型
 * @param variant 按鈕樣式
 * @param color 按鈕顏色
 * @param size 按鈕大小，會調 padding + fontSize
 * @param className 額外 class
 * @param onClick 點擊事件
 * @param ariaLabel html aria-label 屬性
 * @param children ReactNode
 * */
const Button = forwardRef<HTMLButtonElement, ButtonProps>(
  (
    {
      type = 'button',
      variant = 'default',
      color = 'primary',
      size = 'md',
      className,
      onClick,
      ariaLabel,
      children,
    },
    outerRef,
  ) => {
    return (
      <button
        ref={outerRef}
        type={type}
        onClick={onClick}
        className={clsx([
          variant === 'default' && {
            'rounded-md border font-medium transition-all hover:brightness-110 focus:ring-2 active:brightness-125 outline-none': true,
            'border-primary bg-primary text-white ring-primary/40':
              color === 'primary',
            'border-secondary bg-secondary text-white ring-secondary/40': color === 'secondary',
            'gradient text-white ring-primary/40': color === 'gradient',
            'border-red-500 bg-red-500 text-white ring-red-500/40': color === 'red',
            'border-green-500 bg-green-500 text-white ring-green-500/40': color === 'green',
            'border-transparent bg-transparent text-primary ring-transparent':
              color === 'transparent',
            'py-2 px-6 text-xl': size === 'lg',
            'py-2 px-6': size === 'md',
            'py-1 px-3 text-sm': size === 'sm',
          },
          variant === 'header' && {
            'flex-1': true,
            'transition-colors text-xl h-full border-b-4 border-transparent hover:text-primary hover:bg-container-inverse/10': true,
            'md:flex-initial md:w-16': true,
          },
          {
            'text-primary transition-colors hover:text-secondary': variant === 'link',
          },
          className,
        ])}
        aria-label={ariaLabel}
      >
        {children}
      </button>
    )
  },
)
Button.displayName = 'Button'

export default Button
