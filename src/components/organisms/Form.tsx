import { zodResolver } from '@hookform/resolvers/zod'
import type { FormEvent, FunctionComponent, ReactNode } from 'react'
import type { UseFormProps, UseFormReturn } from 'react-hook-form'
import { FormProvider, useForm } from 'react-hook-form'
import type { ZodSchema } from 'zod'

interface FormProps extends UseFormProps {
  schema?: ZodSchema
  onSubmit: (formData: any) => void
  className?: string
  children: ((form: UseFormReturn) => JSX.Element) | ReactNode
}

/**
 * 將 form 統一整合，送出時用 react-hook-form + zod 針對 schema 進行防呆驗證
 *
 * @param schema 需要防呆驗證的 zod 欄位規則，若沒給這個不會觸發任何防呆驗證
 * @param defaultValues 表單欄位預設值
 * @param onSubmit 表單成功通過驗證，送出時要執行的動作
 * @param className 額外 class
 * @param children 可給一般 JSX 或 function 來回傳 JSX，function 可接受 useForm 本身的參數
 * */
const Form: FunctionComponent<FormProps> = ({
  schema,
  defaultValues,
  onSubmit,
  className,
  children,
}) => {
  const UseFormReturn: UseFormReturn = useForm({
    defaultValues,
    resolver: schema && zodResolver(schema),
  })

  // 沒提供 schema 的話，送出會執行這個
  const defaultSubmit = (e: FormEvent) => {
    e.preventDefault()
    onSubmit(UseFormReturn.getValues())
  }

  // 用 schema 是否有提供來判斷，送出時是否走驗證流程；還是單純把 formData 回拋
  const formAction = schema
    ? UseFormReturn.handleSubmit(onSubmit)
    : defaultSubmit

  return (
    <FormProvider {...UseFormReturn} >
      <form className={className} onSubmit={formAction}>
        {typeof children === 'function' ? children(UseFormReturn) : children}
      </form>
    </FormProvider>
  )
}
Form.displayName = 'Form'

export default Form
