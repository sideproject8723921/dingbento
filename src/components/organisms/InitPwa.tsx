import clsx from 'clsx'
import { AnimatePresence, m } from 'framer-motion'
import type { FunctionComponent } from 'react'
import { useCallback, useEffect, useMemo, useState } from 'react'
import { useEventListener } from 'usehooks-ts'

import { Button, Portal } from '@/components/atoms'
import { useDialogContext } from '@/contexts/DialogContext'

/**
 * 初始化 PWA 元件，這裡會註冊 SW，並處理提示安裝(尚未實作)、更新等 UI 顯示
 */
const InitPwa: FunctionComponent = () => {
  const [needUpdate, setNeedUpdate] = useState<boolean | null>(false)
  const { open } = useDialogContext()

  // 判斷瀏覽器環境是否支援 SW
  const swEnable = useMemo(() => typeof window !== 'undefined' && 'serviceWorker' in navigator && window.workbox !== undefined, [])

  // 通知 sw 做事情
  const postSwMessage = useCallback((message: string) => {
    if (swEnable) {
      navigator.serviceWorker.ready.then((registration) => {
        registration.active?.postMessage({ type: message })
      })
    }
  }, [swEnable])

  useEffect(() => {
    if (swEnable) {
      // 以下幾個動作為 service-worker 的生命週期 event，有很多歌能使用，這裡只用到這幾個
      // 參考: https://web.dev/service-worker-lifecycle/
      const wb = window.workbox
      // 有新的 sw 正在安裝
      navigator.serviceWorker.ready.then((registration) => {
        registration.addEventListener('updatefound', () => {
          setNeedUpdate(null) // flag 改 null
        })
      })
      // 新的 sw 已安裝好，等待使用者確認更新
      wb.addEventListener('waiting', () => {
        setNeedUpdate(true) // flag 改 true
      })
      // 使用者已按下更新，新的 sw 已接管網頁
      wb.addEventListener('controlling', () => {
        window.location.reload() // 直接重整頁面
      })
      // 呼叫 sw 註冊
      wb.register()
      // 通知 sw 頁面已開啟
      postSwMessage('APP_OPEN')
    }
  }, [swEnable, open, postSwMessage])

  // 通知 sw 頁面已focus
  useEventListener('focus', () => postSwMessage('APP_FOCUS'))

  return swEnable
    ? (
      <Portal>
        <AnimatePresence>
          <m.div
            key={String(needUpdate)}
            initial={{ opacity: 0, y: 50 }}
            animate={{ opacity: 1, y: 0 }}
            exit={{ opacity: 0, y: 50 }}
            transition={{ duration: 0.15 }}
            className={clsx({
              'absolute bottom-[env(safe-area-inset-bottom)] z-[2] mb-14 flex h-14 w-full items-center justify-between bg-container-inverse px-4 text-sm text-text-inverse md:mb-0': true,
              // flag = false => 不顯示提示
              'hidden': needUpdate === false,
            })}
          >
            {
              // flag = null => 更新中
              needUpdate === null && <span>檢測到有新的版本，正在為您下載，請稍後 ...</span>
            }
            {
              // flag = true => 待使用者確認
              needUpdate === true && (
                <>
                  <span>最新版已下載完畢，請點擊確認進行更新</span>
                  <div className='flex items-center justify-center space-x-1'>
                    <Button ariaLabel='Cancel' className='w-max' size='sm' color='transparent' onClick={() => setNeedUpdate(false)}>
                      取消
                    </Button>
                    <Button ariaLabel='Confirm' className='w-max' size='sm' onClick={() => window.workbox.messageSkipWaiting()}>
                      確認
                    </Button>
                  </div>
                </>
              )
            }
          </m.div>
        </AnimatePresence>
      </Portal>
      )
    : null
}
InitPwa.displayName = 'InitPwa'

export default InitPwa
