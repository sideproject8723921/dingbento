import { useCallback, useMemo, useState } from 'react'

import type { ModalProps } from '@/components/molecules/Modal'
import type { DialogProps } from '@/components/organisms/Dialog'

type UseModalParams = DialogProps & ModalProps

/**
 * 彈窗相關控制邏輯統一處理
 *
 * @returns show 彈窗是否已開啟
 * @returns open 開啟彈窗
 * @returns close 關閉彈窗
 * @returns Modal 基礎彈窗 component (molecules/Modal)
 * @returns Dialog 提示彈窗 component (organisms/Dialog)
 * @returns props Modal/Dialog 需要的 props (方便用 ...props 設定)
 */
function useModal(params?: UseModalParams) {
  const [show, setShow] = useState(false)

  const open = useCallback(() => setShow(true), [])
  const close = useCallback(() => setShow(false), [])
  const optionalProps = useMemo(() => params || ({}), [params])

  return {
    show,
    open,
    close,
    props: {
      show,
      close,
      ...optionalProps,
    },
  }
}

export default useModal
