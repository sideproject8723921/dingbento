import axios from 'axios'
import { useRouter } from 'next/router'
import type { ReactNode } from 'react'
import { useEffect, useMemo } from 'react'
import { SWRConfig } from 'swr'

import { useLoadingContext } from '@/contexts/LoadingContext'

interface AxiosInterceptorProps {
  children: ReactNode
  fallback: Record<string, any>
}

const host = process.env.NEXT_PUBLIC_API_URL || 'http://localhost:3000'
const instance = axios.create({
  baseURL: `${host}/api/`,
})

// 先寫不需要 context 的 interceptor，SSR 才能吃到
instance.interceptors.request.use((request) => {
  // log 所有請求
  console.log(
    `%c[${request.method?.toUpperCase()} ${request.url}]:`,
    'color: green; font-weight: bold',
  )
  console.log(request.params || request.data)
  return request
})
instance.interceptors.response.use((response) => {
  // log 所有回應
  console.log(
    `%c[${response.config?.method?.toUpperCase()} ${response.config?.url}]:`,
    'color: blue; font-weight: bold',
  )
  console.log(response.data)
  // 移除 axios 多餘的 data 層
  return response.data
})

// 這裡是 axios 中間層，寫成類似 context 的方式以便能使用其他 context
function AxiosInterceptor({ children, fallback }: AxiosInterceptorProps) {
  const { dispatch } = useLoadingContext()
  const router = useRouter()
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const controller = useMemo(() => new AbortController(), [router.asPath])

  useEffect(() => {
    // 發出請求前
    const reqInterceptor = instance.interceptors.request.use((request) => {
      request.signal = controller.signal
      dispatch({ type: 'triggerLoading', payload: true })
      return request
    }, (error) => {
      dispatch({ type: 'triggerLoading', payload: false })
      return Promise.reject(error)
    })

    // 收到回應後
    const resInterceptor = instance.interceptors.response.use((response) => {
      dispatch({ type: 'triggerLoading', payload: false })
      return response
    }, (error) => {
      dispatch({ type: 'triggerLoading', payload: false })
      return Promise.reject(error)
    })

    return () => {
      instance.interceptors.request.eject(reqInterceptor)
      instance.interceptors.response.eject(resInterceptor)
    }
  }, [dispatch, controller])

  useEffect(() => {
    // 路徑切換時，打消當前的 API 呼叫
    const abortRequest = () => controller.abort()
    router.events.on('routeChangeStart', abortRequest)
    return () => {
      router.events.off('routeChangeStart', abortRequest)
    }
  }, [router.events, controller])

  return (
    <SWRConfig value={{
      revalidateOnFocus: false,
      revalidateOnMount: false, // 走 ssr，載入頁面時都不呼叫 api
      fetcher: instance,
      fallback,
    }}>
      {children}
    </SWRConfig>
  )
}

export default instance
export { AxiosInterceptor }
