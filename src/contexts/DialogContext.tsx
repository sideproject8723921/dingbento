import type { ReactNode } from 'react'
import { createContext, useCallback, useContext, useState } from 'react'

import type { DialogProps } from '@/components/organisms/Dialog'
import { useModal } from '@/hooks'

// 類型宣告
interface DialogContextType {
  props: DialogProps
  open: (params: DialogContextType['props']) => void
  close: () => void
}

// 預設值
const defaultValues: DialogContextType = {
  props: {
    variant: 'primary',
    title: undefined,
    message: undefined,
  },
  open: () => {},
  close: () => {},
}

// 建立 context
const DialogContext = createContext<DialogContextType>(defaultValues)

// 外拋 context hook
export function useDialogContext() {
  return useContext(DialogContext)
}

// 外拋 context provider
export function DialogProvider({ children }: { children: ReactNode }) {
  // TODO: 目前一次只能顯示一個 dialog，這裡若用 array + index 的方式，應該可以調整成可 dialog 重疊的概念
  const [props, setProps] = useState<DialogContextType['props']>(defaultValues.props)
  // 使用 useModal hook，並綁定 onClose
  const { show, open: openDialog, close: closeDialog } = useModal(
    {
      ...props,
      onClose: () => setProps(defaultValues.props),
    })

  // open 時，更新 props；在呼叫 useModal 的 open()，會用最新的 props 顯示提示彈窗
  const open = useCallback((params: DialogContextType['props']) => {
    setProps(params)
    openDialog()
  }, [openDialog])

  // close 時，單純呼叫 useModal 的 close() 關閉彈窗；待彈窗完全關閉後會觸發上方綁定的 onClose 將 props 設定為預設值
  const close = useCallback(() => {
    closeDialog()
  }, [closeDialog])

  // const {  open, close, props } = useDialogContext()
  // useDialogContext 能取得的參數，用 open({ variant:'primary', title:'標題', message: '內容' }) 即可叫出提示彈窗
  const value = {
    props: {
      ...props,
      show,
      close,
    },
    open,
    close,
  }

  return (
    <DialogContext.Provider value={value}>
      {children}
    </DialogContext.Provider>
  )
}
