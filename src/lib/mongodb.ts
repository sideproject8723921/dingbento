import type { MongoClientOptions } from 'mongodb'
import { MongoClient } from 'mongodb'

// mongodb 連線字串
if (!process.env.MONGODB_URI)
  throw new Error('Missing env variable: MONGODB_URI')
if (!process.env.MONGODB_DB_NAME)
  throw new Error('Missing env variable: MONGODB_DB_NAME')
const uri = process.env.MONGODB_URI + process.env.MONGODB_DB_NAME

// mongodb 參數
const options: MongoClientOptions = {
  maxIdleTimeMS: 10000,
  socketTimeoutMS: 20000,
  retryWrites: true,
  writeConcern: {
    w: 'majority',
  },
}

let client
// eslint-disable-next-line import/no-mutable-exports
let clientPromise: Promise<MongoClient>

if (process.env.NODE_ENV === 'development') {
  // In development mode, use a global variable so that the value
  // is preserved across module reloads caused by HMR (Hot Module Replacement).
  const globalWithMongo = global as typeof globalThis & {
    _mongoClientPromise?: Promise<MongoClient>
  }

  if (!globalWithMongo._mongoClientPromise) {
    client = new MongoClient(uri, options)
    globalWithMongo._mongoClientPromise = client.connect()
  }
  clientPromise = globalWithMongo._mongoClientPromise
}
else {
  // In production mode, it's best to not use a global variable.
  client = new MongoClient(uri, options)
  clientPromise = client.connect()
}

// Export a module-scoped MongoClient promise. By doing this in a
// separate module, the client can be shared across functions.
export default clientPromise
