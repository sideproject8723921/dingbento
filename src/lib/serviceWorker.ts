/// <reference lib="webworker" />
import { cleanupOutdatedCaches, precacheAndRoute } from 'workbox-precaching'

import { getIdbValue, setIdbValue } from '@/lib/idb'

interface ExtendedServiceWorkerGlobalScope extends ServiceWorkerGlobalScope {
  navigator:
  WorkerNavigator &
  Record<'setAppBadge', (count?: number) => Promise<void>> &
  Record<'clearAppBadge', () => Promise<void>>
}

declare let self: ExtendedServiceWorkerGlobalScope

// 新增紅點的 function
async function addBadge() {
  if (self.navigator.setAppBadge) {
    // 判斷紅點應該顯示多少 (存在 idb)
    const count = await getIdbValue('badgeCount') + 1
    await setIdbValue('badgeCount', count)
    // 設定標記的 promise
    await self.navigator.setAppBadge(count)
  }
}

// 移除所有通知和紅點
async function clearNotificationAndBadge() {
  // NOTE: IOS getNotifications() 目前會給空陣列，故無法清除其他通知
  const notifications = await self.registration.getNotifications()
  if (notifications.length) {
    notifications.forEach((notification) => {
      notification.close()
    })
  }
  if (self.navigator.clearAppBadge) {
    await setIdbValue('badgeCount', 0)
    await self.navigator.clearAppBadge()
  }
}

// focus 或開新視窗
async function focusOrNewWindow() {
  await self.clients.matchAll({ type: 'window' }).then((clientsArr) => {
    // If a Window tab matching the targeted URL already exists, focus that;
    const hadWindowToFocus = clientsArr.some(windowClient =>
      windowClient.url.includes(self.registration.scope)
        ? (windowClient.focus(), true)
        : false,
    )
    // Otherwise, open a new tab to the applicable URL and focus it.
    if (!hadWindowToFocus) {
      self.clients
        .openWindow(self.registration.scope)
        .then(windowClient => (windowClient ? windowClient.focus() : null))
    }
  })
}

// sw 被主程式通知
self.addEventListener('message', (event) => {
  switch (event.data?.type) {
    case 'SKIP_WAITING':
      self.skipWaiting()
      break
    case 'APP_OPEN':
    case 'APP_FOCUS':
      event.waitUntil(clearNotificationAndBadge())
      break
    default:
      break
  }
})

// 推播設定
self.addEventListener('push', async (event) => {
  const promises: Promise<void>[] = []

  // 後台推播的資料
  let data = {
    title: 'New message coming!',
    content: 'Message content!',
  }
  if (event.data)
    data = JSON.parse(event.data.text())
  // 通知的設定、內容等
  const options: NotificationOptions = {
    // 樣式、內容設定
    body: data.content,
    icon: '/icon/apple-icon-180.png',
    // image: '/icon/logo.png',
    badge: '/icon/favicon.svg',
    // 操作設定
    actions: [
      {
        action: 'confirm',
        title: '前往',
      },
      {
        action: 'cancel',
        title: '取消',
      },
    ],
    data,
    // 行為設定
    // tag: 'demo',
    // requireInteraction: '<boolean>',
    // renotify: true,
    // sound: '<URL String>',
    // silent: '<Boolean>',
    vibrate: [200, 100, 200],
  }
  // 新增 icon 紅點標記
  promises.push(addBadge())
  // 推播的 promise
  promises.push(self.registration.showNotification(data.title, options))
  // 讓 sw 等待 promises 都 resolved 才關閉
  event.waitUntil(Promise.all(promises))
})

// 通知點擊行為
self.addEventListener('notificationclick', (event) => {
  if (!event.action) {
    // 直接點在通知上 (通常不做任何處理)
    console.log('使用者點擊通知區域')
    return
  }
  // 點在按鈕上，用 action 當 key 判斷
  switch (event.action) {
    case 'confirm':
      console.log('使用者點擊確認')
      console.log(event.notification.data)
      event.waitUntil(focusOrNewWindow())
      break
    case 'cancel':
      console.log('使用者點擊取消')
      event.notification.close()
      break
    default:
      console.log(`未定義 Action: '${event.action}'`)
      break
  }
})

cleanupOutdatedCaches()
precacheAndRoute(self.__WB_MANIFEST)
