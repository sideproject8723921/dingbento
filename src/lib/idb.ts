import { openDB } from 'idb'

import app from '../../package.json'

// 瀏覽器的 IndexedDB
const idbStoreName = 'app'
const dbPromise = openDB(app.name, 1, {
  upgrade(db) {
    db.createObjectStore(idbStoreName)
  },
})

// 外拋直接操作 idb 的方式，主要 sw 要用
export async function getIdbValue(k: string) {
  return (await dbPromise).get(idbStoreName, k)
}
export async function setIdbValue(k: string, v: any) {
  return (await dbPromise).put(idbStoreName, v, k)
}
