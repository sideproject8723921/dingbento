import type { ObjectId } from 'mongodb'
import type { PushSubscription } from 'web-push'

import clientPromise from '@/lib/mongodb'

export interface Subscription extends PushSubscription {
  // mongodb id
  _id?: ObjectId
  // 使用者唯一值，帳號、電話、email 之類的
  userid: string
  // 進行推播時，是否跳過此設備的 flag (理論上，使用者登出就不再推送之類的)
  skip: boolean
}

export default async function subscription() {
  const client = await clientPromise
  return client.db().collection<Subscription>('subscriptions')
}
