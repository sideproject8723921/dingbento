import type { ObjectId } from 'mongodb'

import clientPromise from '@/lib/mongodb'

export interface Restaurant {
  // mongodb id
  _id?: ObjectId
  // 店家名稱
  name: string
  // 電話
  telno: string
  // 地址
  address?: string
  // 菜單
  menu: Array<{
    item: string
    price: number
  }>
}

export default async function restaurant() {
  const client = await clientPromise
  return client.db().collection<Restaurant>('restaurants')
}
