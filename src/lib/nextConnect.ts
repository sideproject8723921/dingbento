import type { NextApiRequest, NextApiResponse } from 'next'
import { createRouter } from 'next-connect'

// 將 nc router 封裝，統一幾個全域動作
function nc() {
  const router = createRouter<NextApiRequest, NextApiResponse>()
  const handler = router.handler.bind(router)

  router
    .use(async (req, res, next) => {
      // TODO: 可以 log 使用者的資訊、日期時間、token 狀態等
      const { url, body, method, query } = req
      console.log({
        timestamp: new Date().toLocaleString('zh-TW', { hour12: false }),
        request: `${method} => ${url}`,
        payload: body || query,
      })
      await next()
      // TODO: 看有沒有辦法統一 log 所有的 API 回應
      // res.on('finish', async () => {
      //   const {} = res
      //   console.log({
      //     timestamp: new Date().toLocaleString('zh-TW', {
      //       hour12: false,
      //     }),
      //     response: `${method} => ${url}`,
      //     payload: 'Response data from api handler',
      //   })
      // })
    })

  router.handler = options => handler({
    // 查無 api method 統一判斷
    onNoMatch: (req, res) => {
      // 一律通行 options preflight，否則 localhost cors 不能過
      if (req.method === 'OPTIONS')
        res.status(200).end()
      else
        res.json({ retCode: 1, retMsg: `Method ${req.method} Not Allowed` })
    },
    // 未知錯誤統一輸出
    onError: (err: any, req, res) => {
      console.log(err)
      res.json({ retCode: 1, retMsg: 'Internal server error', retData: err.toString() })
    },
    // 可在 api handler 那丟參數覆蓋
    ...options,
  })

  return router
}

export default nc
