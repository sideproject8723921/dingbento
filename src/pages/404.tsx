export default function Custom404() {
  return (
    <div className='flex h-full w-full flex-col items-center justify-center space-y-2'>
      <h1 className="text-xl">404 - Page Not Found</h1>
    </div>
  )
}
