import type { Table as ReactTable } from '@tanstack/react-table'
import { createColumnHelper } from '@tanstack/react-table'
import clsx from 'clsx'
import { useCallback, useMemo, useRef, useState } from 'react'
import useSWR from 'swr'

import { Button } from '@/components/atoms'
import { Table } from '@/components/organisms'
import type { PushModalProps } from '@/components/templates/subscription/PushModal'
import PushModal from '@/components/templates/subscription/PushModal'
import axios from '@/contexts/ApiContext'
import { useDialogContext } from '@/contexts/DialogContext'
import { useModal } from '@/hooks'
import type { SubscriptionIdPutRequest, SubscriptionPushPostRequest } from '@/types/api-request'
import type { ApiResponse, SubscriptionGetResponse, SubscriptionType } from '@/types/api-response'

// SSR 訂閱資料
export async function getServerSideProps() {
  const subscription = await axios('subscription')
  return {
    props: { fallback: { subscription } },
  }
}

export default function HomePage() {
  // 初始化 dialog
  const dialog = useDialogContext()

  // ----------- table ----------
  // 展開推播網址編號
  const [expandedId, setExpandedId] = useState('')

  // 載入訂閱資料，呼叫 mutate 會自動呼叫取新資料
  const { data, mutate } = useSWR<ApiResponse<SubscriptionGetResponse>>('subscription')
  const tableData = useMemo(() => data?.retCode === 0 ? data.retData : [], [data])

  // 切換是否暫停推播
  const toggleSkip = useCallback(async (row: SubscriptionType) => {
    const { retCode } = await axios.put<{}, SubscriptionIdPutRequest>(`subscription/${row.id}`, { skip: !row.skip })
    if (retCode === 0)
      mutate()
  }, [mutate])

  // 刪除此筆資料
  const handleDelete = useCallback(async (row: SubscriptionType) => {
    dialog.open({
      message: `確定刪除 ${row.id} 此筆資料?`,
      onConfirm: async (closeDialog) => {
        const { retCode } = await axios.delete(`subscription/${row.id}`)
        if (retCode === 0) {
          closeDialog()
          mutate()
        }
      },
    })
  }, [mutate, dialog])

  // table 欄位
  const { accessor } = createColumnHelper<SubscriptionType>()
  const columns = useMemo(
    () => [
      accessor('id', {
        header: '訂閱編號',
      }),
      accessor('userid', {
        header: '帳號',
      }),
      accessor(row => row, {
        id: 'endpoint',
        header: () => (
          <>
            <span>推播網址 </span>
            <small className='text-blue-400'>(點擊展開)</small>
          </>
        ),
        cell: ({ row }) => {
          const { id, endpoint } = row.original
          return (
            <div
              className={clsx({
                'm-auto max-w-[200px] cursor-pointer group': true,
                'overflow-hidden text-ellipsis whitespace-nowrap': expandedId !== id,
                'break-all': expandedId === id,
              })}
              onClick={() => setExpandedId(expandedId === id ? '' : id)}
            >
              {endpoint}
            </div>
          )
        },
      }),
      accessor('skip', {
        header: '狀態',
        cell: ({ getValue }) => <span>{getValue() ? '已停用' : '使用中'}</span>,
      }),
      accessor(row => row, {
        id: 'actions',
        header: '操作',
        cell: ({ row }) => {
          const values = row.original
          return (
            <div className='flex items-center justify-center space-x-1'>
              {
                values.skip
                  ? <Button ariaLabel='Recover' size='sm' onClick={() => toggleSkip(values)}>恢復</Button>
                  : <Button ariaLabel='Stop' size='sm' color='secondary' onClick={() => toggleSkip(values)}>停用</Button>
              }
              <Button ariaLabel='Delete' color='red' size='sm' onClick={() => handleDelete(values)}>刪除</Button>
            </div>
          )
        },
      }),
    ],
    [accessor, expandedId, toggleSkip, handleDelete],
  )

  // 取得 table instance
  const tableRef = useRef<ReactTable<SubscriptionType>>(null)
  // ----------- end of table ----------

  // ----------- modal ----------
  const { open, close, props } = useModal({ title: '訊息推播' })

  // 開啟彈窗，必須先防呆至少選擇一筆資料
  const openModal = useCallback(() => {
    if (!tableRef.current?.getSelectedRowModel().flatRows.length) {
      dialog.open({
        variant: 'danger',
        message: '請先選擇至少一筆資料',
      })
    }
    else { open() }
  }, [dialog, open])

  // 進行推播
  const onSubmit: PushModalProps['onSubmit'] = useCallback(async ({ message }) => {
    // 透過 ref 取得 table 已勾選的資料
    const ids = tableRef.current?.getSelectedRowModel().flatRows.map(r => r.original.id)
    if (!ids) {
      return dialog.open({
        variant: 'danger',
        message: '請先選擇至少一筆資料',
      })
    }
    const { retCode, retMsg } = await axios.post<{}, SubscriptionPushPostRequest>('subscription/push', { ids, message })
    // 彈窗成功或失敗
    dialog.open({
      variant: retCode === 0 ? 'success' : 'danger',
      message: retMsg,
    })
    // 成功的話順便關掉 PushModal
    if (retCode === 0)
      close()
  }, [dialog, close])
  // ----------- end of modal ----------

  return (
    <>
      <Table
        tableRef={tableRef}
        data={tableData}
        columns={columns}
        enableRowSelection={row => !row.original.skip}
      />
      <Button ariaLabel='Push notification' className='mx-auto mt-3 w-max' onClick={openModal}>推播訊息</Button>
      <PushModal {...props} onSubmit={onSubmit} />
    </>
  )
}
