import { useCallback, useMemo } from 'react'
import useSWR from 'swr'

import { Button } from '@/components/atoms'
import { Card } from '@/components/molecules'
import type { UploadModalProps } from '@/components/templates/restaurant/UploadModal'
import UploadModal from '@/components/templates/restaurant/UploadModal'
import axios from '@/contexts/ApiContext'
import { useDialogContext } from '@/contexts/DialogContext'
import { useModal } from '@/hooks'
import type { RestaurantPostRequest } from '@/types/api-request'
import type { ApiResponse, RestaurantGetResponse } from '@/types/api-response'

// SSR 店家資料
export async function getServerSideProps() {
  const restaurant = await axios('restaurant')
  return {
    props: { fallback: { restaurant } },
  }
}

export default function RestaurantPage() {
  const image = '/icon/manifest-icon-512.maskable.png'

  const { data, mutate } = useSWR<ApiResponse<RestaurantGetResponse>>('restaurant')
  const restaurantData = useMemo(() => (data?.retCode === 0 && data.retData) ? data.retData : [], [data])

  const dialog = useDialogContext()
  const { open, close, props } = useModal({ title: '新增店家' })

  // 新增店家
  const onSubmit: UploadModalProps['onSubmit'] = useCallback(async ({ name, telno, address, fileData }) => {
    const { retCode, retMsg } = await axios.post<{}, RestaurantPostRequest>(
      'restaurant',
      {
        name,
        telno,
        address,
        menu: fileData,
      })

    dialog.open({
      variant: retCode === 0 ? 'success' : 'danger',
      message: retMsg,
    })

    if (retCode === 0) {
      mutate()
      close()
    }
  }, [dialog, close, mutate])

  return (
    <>
      <Button ariaLabel="Add restaurant" className='ml-auto' onClick={open}>新增店家</Button>
      <div className='mt-2 grid grid-cols-2 gap-2 overflow-y-auto pb-4 md:grid-cols-3 md:gap-4 lg:grid-cols-4'>
        {
          restaurantData.map(({ name, telno }, index) => (
            <Card image={image} key={index}>
              <div className='flex items-center justify-between'>
                <div className='flex flex-col'>
                  <b>{name}</b>
                  <small>{telno}</small>
                </div>
                <small className='flex aspect-square w-10 items-center justify-center rounded-full bg-container-inverse/10 font-semibold'>
                  4.5
                </small>
              </div>
            </Card>
          ))
        }
      </div>
      <UploadModal {...props} onSubmit={onSubmit} />
    </>
  )
}
