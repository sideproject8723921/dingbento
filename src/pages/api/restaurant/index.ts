import restaurant from '@/lib/models/restaurant'
import nc from '@/lib/nextConnect'

const router = nc()

router
  // 查詢所有店家
  .get(async (req, res) => {
    const collection = await restaurant()

    const result = (await collection.find().toArray())
      .map(({ _id, name, telno, address }) => ({
        id: _id,
        name,
        telno,
        address,
      }))

    if (result)
      res.json({ retCode: 0, retMsg: '查詢成功', retData: result })
    else res.json({ retCode: 1, retMsg: '查詢失敗' })
  })

  // 新增店家
  .post(async (req, res) => {
    const { name, telno, address, menu } = req.body
    const collection = await restaurant()

    const result = await collection.insertOne({ name, telno, address, menu })

    if (result.acknowledged && result.insertedId)
      res.json({ retCode: 0, retMsg: `成功新增: ${name}` })
    else res.json({ retCode: 1, retMsg: '新增失敗' })
  })

export default router.handler()
