import { ObjectId } from 'mongodb'

import subscription from '@/lib/models/subscription'
import nc from '@/lib/nextConnect'

const router = nc()

router
  // 取消訂閱
  .delete(async (req, res) => {
    const { id } = req.query
    const collection = await subscription()

    const result = await collection.deleteOne(
      { _id: new ObjectId(id as string) },
    )
    if (result.acknowledged)
      res.json({ retCode: 0, retMsg: '取消訂閱成功' })
    else res.json({ retCode: 1, retMsg: '取消訂閱失敗' })
  })

  // 暫停、啟用訂閱推播 (改 flag)
  .put(async (req, res) => {
    const { id } = req.query
    const skip = Boolean(req.body.skip)
    const collection = await subscription()

    const result = await collection.updateOne(
      { _id: new ObjectId(id as string) },
      { $set: { skip } },
    )
    if (result.acknowledged)
      res.json({ retCode: 0, retMsg: `已${skip ? '暫停' : '恢復'}推播功能` })
    else res.json({ retCode: 1, retMsg: `${skip ? '暫停' : '恢復'}推播功能失敗` })
  })

export default router.handler()
