import subscription from '@/lib/models/subscription'
import nc from '@/lib/nextConnect'

const router = nc()

router
  // 查詢所有訂閱
  .get(async (req, res) => {
    const collection = await subscription()

    const result = (await collection.find().toArray())
      .map(({ _id, userid, skip, endpoint }) => ({
        id: _id,
        userid,
        skip,
        endpoint,
      }))

    if (result)
      res.json({ retCode: 0, retMsg: '查詢成功', retData: result })
    else res.json({ retCode: 1, retMsg: '查詢失敗' })
  })

  // 新增訂閱
  .post(async (req, res) => {
    const { userid, endpoint, keys } = req.body
    const collection = await subscription()

    const result = await collection.insertOne({
      userid,
      endpoint,
      keys,
      skip: false,
    })

    if (result.acknowledged && result.insertedId)
      res.json({ retCode: 0, retMsg: '訂閱成功', retData: { id: result.insertedId } })
    else res.json({ retCode: 1, retMsg: '訂閱失敗' })
  })

export default router.handler()
