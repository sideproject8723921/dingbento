// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'

interface Data {
  name: string
}

const nameArr = ['John Doe', 'Michael', 'David', 'Mike']

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>,
) {
  const { id } = req.query
  // 測試用
  // if (id !== '3')
  await new Promise(resolve => setTimeout(() => resolve('done'), 100))
  // else
  //   await new Promise(resolve => setTimeout(() => resolve('done'), 3000))
  res.status(200).json({ name: nameArr[parseInt(id as string)] })
}
