// PUT => subscription/${id}
export interface SubscriptionIdPutRequest {
  skip: boolean
}

// POST => subscription/push
export interface SubscriptionPushPostRequest {
  ids: string[]
  message: string
}

// POST => restaurant
export interface RestaurantPostRequest {
  name: string
  telno: string
  address: string
  menu: Array<{
    item: string
    price: number
  }>
}
