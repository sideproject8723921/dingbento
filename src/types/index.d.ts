/// <reference lib="webworker" />
import { ApiResponse } from '@/types/api-response'

// 定義預處理 axios 後的 type
declare module "axios" {  
  export interface AxiosInstance {
    request<T>(config: AxiosRequestConfig): Promise<ApiResponse<T>>;
    get<T>(url: string, config?: AxiosRequestConfig): Promise<ApiResponse<T>>;
    delete<T>(url: string, config?: AxiosRequestConfig): Promise<ApiResponse<T>>;
    head<T>(url: string, config?: AxiosRequestConfig): Promise<ApiResponse<T>>;
    post<T, K>(
      url: string,
      data?: K,
      config?: AxiosRequestConfig
    ): Promise<ApiResponse<T>>;
    put<T, K>(
      url: string,
      data?: K,
      config?: AxiosRequestConfig
    ): Promise<ApiResponse<T>>;
    patch<T, K>(
      url: string,
      data?: K,
      config?: AxiosRequestConfig
    ): Promise<ApiResponse<T>>;
  }
}

declare global {
  interface Window {
    workbox: ServiceWorkerGlobalScope & {
      messageSkipWaiting(): void
      register(): void
    }
  }
}