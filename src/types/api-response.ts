export interface ApiResponse<T> {
  retCode: 0 | 1 // 0: 成功 、 1: 失敗
  retMsg?: string
  retData?: T
}

export interface SubscriptionType {
  id: string
  userid: string
  skip: boolean
  endpoint: string
}

// GET => subscription
export interface SubscriptionGetResponse extends Array<SubscriptionType> {}

export interface RestaurantType {
  name: string
  telno: string
  address: string
  menu: Array<{
    item: string
    price: number
  }>
}

// GET => restaurant
export interface RestaurantGetResponse extends Array<RestaurantType> {}
