/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx}',
    './src/components/**/*.{js,ts,jsx,tsx}',
    './src/app/**/*.{js,ts,jsx,tsx}',
  ],
  theme: {
    extend: {
      colors: {
        primary: 'rgba(var(--c-brand), <alpha-value>)',
        secondary: 'rgba(var(--c-brand-next), <alpha-value>)',
        // 暗亮色系相關
        text: {
          normal: 'rgba(var(--c-text), <alpha-value>)',
          inverse: 'rgba(var(--c-text-inverse), <alpha-value>)',
        },
        container: {
          normal: 'rgba(var(--c-bg), <alpha-value>)',
          inverse: 'rgba(var(--c-bg-inverse), <alpha-value>)',
        },
      },
    },
  },
  plugins: [require('@tailwindcss/forms')],
}
